<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Page</title>
    <style type="text/css">
        textarea {
            resize: none;
        }
    </style>
</head>
<body>
    <p>Kembali ke Halaman <a href="/">Sebelumnya</a></p>
    <hr>
    <h1>Buat Akun Baru</h1>
    <fieldset>
        <legend><h2>Sign Up Form</h2></legend>
        <form action="/welcome" method="post" autocomplete="off">
            @csrf
            <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname" required><br><br>
            <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname" required><br><br>

            <label for="gender">Gender</label><br><br>
            <input type="radio" id="male" name="jk" value="Male" required>
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="jk" value="Female" required>
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="jk" value="Other" required>
            <label for="other">Other</label><br><br>

            <label for="browser">Nationality :</label><br><br>
            <input list="test" name="ntn" id="browser" required>
            <datalist id="test">
              <option value="Indonesian">
              <option value="Singaporian">
              <option value="Malaysian">
              <option value="philiphines">
              <option value="Australian">
            </datalist>
            <br><br>

            <label for="language"> Language Spoken : </label><br><br>
            <input type="checkbox"  name="indo" value="Indonesia" >
            <label for="lang1"> Bahasa Indonesia</label><br>
            <input type="checkbox"  name="english" value="English" >
            <label for="lang2"> English</label><br>
            <input type="checkbox"  name="other" value="Other" >
            <label for="lang3"> Other</label><br><br>

            <label for="bio">Bio:</label><br><br>

            <textarea id="bio" name="bio" rows="4" cols="50" required></textarea><br><br>

            <input type="submit" value="Sign Up">
        </form>
    </fieldset>
</body>
</html>
