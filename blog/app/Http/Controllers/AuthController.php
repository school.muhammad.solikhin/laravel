<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function signUp()
    {
        return view('page/signUp');
    }

    function welcome(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('page/welcome', compact('fname','lname'));
    }
}
