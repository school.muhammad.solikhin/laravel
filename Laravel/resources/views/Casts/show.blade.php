@extends('adminlte.master')
@section('title')
<h2>Detail Casts id {{$cast->id}}</h2>
@endsection
@section('content')
<a href="/cast" class="btn btn-primary mb-2">Kembali</a>
    <h3>ID : {{$cast->id}}</h3>
    <h3>Nama : {{$cast->nama}}</h3>
    <h3>Umur : {{$cast->umur}}</h3>
    <h3>Bio : {{$cast->bio}}</h3>
@endsection
